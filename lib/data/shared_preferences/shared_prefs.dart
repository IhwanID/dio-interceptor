import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class SharedPref {
  SharedPreferences _sharedPreferences;

  SharedPref(this._sharedPreferences);

  String getToken() {
    return _sharedPreferences.getString(_tokenKey);
  }

  Future<bool> saveToken(String token) {
    return _sharedPreferences.setString(_tokenKey, token);
  }

  Future<bool> removeToken(String token) {
    return _sharedPreferences.remove(_tokenKey);
  }
}

const _tokenKey = "token";
