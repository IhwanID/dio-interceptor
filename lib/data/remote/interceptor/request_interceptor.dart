import 'dart:async';
import 'package:dio/dio.dart';
import 'package:flutter_dio_interceptor/data/remote/interceptor/logging_interceptor.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RequestInterceptor {
  LoggingInterceptor _loggingInterceptor;

  RequestInterceptor(this._loggingInterceptor);

  Future<Options> getRequestInterceptor(Options requestOptions) async {
    return requestOptions;
  }

  dynamic requestInterceptor(RequestOptions options) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.get("token");

    options.headers.addAll({"Token": "$token"});

    _loggingInterceptor.printRequest(options);
    return options;
  }
}
