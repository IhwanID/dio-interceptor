import 'package:dio/dio.dart';
import 'package:flutter_dio_interceptor/data/remote/interceptor/logging_interceptor.dart';

class ResponseInterceptor {
  LoggingInterceptor _loggingInterceptor;

  ResponseInterceptor(this._loggingInterceptor);

  Response getResponseInterceptor(Response response) {
    _loggingInterceptor.printSuccess(response);
    return response;
  }
}
