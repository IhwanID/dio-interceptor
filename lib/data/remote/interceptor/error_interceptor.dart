import 'package:dio/dio.dart';
import 'package:flutter_dio_interceptor/data/remote/interceptor/logging_interceptor.dart';

class ErrorInterceptor {
  LoggingInterceptor _loggingInterceptor;

  ErrorInterceptor(this._loggingInterceptor);

  DioError getErrorInterceptors(DioError dioError) {
    if (dioError.message.contains("ERROR_001")) {
      //go to login page
    }
    _loggingInterceptor.printError(dioError);
    return dioError;
  }
}
