import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_dio_interceptor/data/remote/interceptor/error_interceptor.dart';
import 'package:flutter_dio_interceptor/data/remote/interceptor/logging_interceptor.dart';
import 'package:flutter_dio_interceptor/data/remote/interceptor/request_interceptor.dart';
import 'package:flutter_dio_interceptor/data/remote/interceptor/response_interceptor.dart';

Future<Dio> createDio(
    BaseOptions baseConfiguration,
    ErrorInterceptor errorInterceptor,
    ResponseInterceptor responseInterceptor,
    RequestInterceptor requestInterceptor) async {
  var dio = Dio(baseConfiguration);
  dio.interceptors.add(InterceptorsWrapper(
      onRequest: (RequestOptions options) async =>
          await requestInterceptor.getRequestInterceptor(options),
      onResponse: (Response response) =>
          responseInterceptor.getResponseInterceptor(response),
      onError: (DioError dioError) =>
          errorInterceptor.getErrorInterceptors(dioError)));
  return dio;
}

BaseOptions createDioOptions(
    String baseUrl, int connectionTimeout, int connectionReadTimeout) {
  return BaseOptions(
    baseUrl: baseUrl,
    connectTimeout: connectionTimeout,
    receiveTimeout: connectionReadTimeout,
  );
}

LoggingInterceptor getLoggingInterceptor() {
  return LoggingInterceptor();
}

ErrorInterceptor getErrorInterceptor(LoggingInterceptor loggingInterceptor) {
  return ErrorInterceptor(loggingInterceptor);
}

ResponseInterceptor getResponseInterceptor(
    LoggingInterceptor loggingInterceptor) {
  return ResponseInterceptor(loggingInterceptor);
}

RequestInterceptor getRequestInterceptor(
    LoggingInterceptor loggingInterceptor) {
  return RequestInterceptor(loggingInterceptor);
}
